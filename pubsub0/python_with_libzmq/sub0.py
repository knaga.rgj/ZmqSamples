#!/usr/bin/env python3

import zmq
import time

def main(useWebSocket=False):

    port = 2001

    if useWebSocket:
        proto = "ws"
    else:
        proto = "tcp"
        pass
    
    ctx = zmq.Context()
    sub = ctx.socket(zmq.SUB)
    sub.connect(f"{proto}://localhost:{port}")
    sub.setsockopt_string(zmq.SUBSCRIBE, '')

    while True:
        msg =sub.recv_string()
        print(msg)
        pass

    return


if __name__=='__main__':
    main(useWebSocket=False)
    #main(useWebSocket=True)
    pass

