#!/usr/bin/env python3

import zmq
import time

def main(useProxy = True, useWebSocket = False):

    if useProxy:
        port = 2000
    else:
        port = 2001
        pass

    if useWebSocket:
        proto = "ws"
    else:
        proto = "tcp"
        pass
    
    ctx = zmq.Context()
    pub = ctx.socket(zmq.PUB)

    if useProxy:
        pub.connect(f"{proto}://localhost:{port}")
    else:
        pub.bind(f"{proto}://*:{port}")
        pass

    count=0

    while True:
        msg = "Pyzmq/pub0 %d"%(count)
        pub.send_string(msg)
        count += 1
        print(msg)
        time.sleep(0.5)
        pass

    return


if __name__=='__main__':
    main(useProxy=True, useWebSocket=False)
    #main(useProxy=False, useWebSocket=False)
    #main(useProxy=True, useWebSocket=True)
    pass
