
# ZmqSamples/pubsub0/python_with_libzmq

## tested

ubuntu
  | version | codename | zmq/tcp | zmq/WebSocket | libzmq5 | libzmq5.so |
  | :-:     | :-:      | :-:     | :-:           | :-:     |            |
  | 18.04   | bionic   | ○      | ×            | 4.2.5-1 | 5.1.5      |
  | 20.04   | focal    | ○      | × (*1)       | 4.3.2-2 | 5.2.2      |
  | 22.04   | jammy    | ○      | ○            | 4.3.4-2 | 5.2.4      |
  | 24.04   | noble    | ○      | ○            | 4.3.5-1 |            |

 (*1) If you use jammy's libzmq5 backported for focal, it will work.
