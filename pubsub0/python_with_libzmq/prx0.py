#! /usr/bin/env python3

import zmq


def main(useWebSocket=False):

    xsub_port = 2000
    xpub_port = 2001

    if useWebSocket:
        proto = "ws"
    else:
        proto = "tcp"
        pass

    ctx = zmq.Context()

    # Socket facing producers
    xsub = ctx.socket(zmq.XSUB)
    xsub.bind(f"{proto}://*:{xsub_port}")

    # Socket facing consumers
    xpub = ctx.socket(zmq.XPUB)
    xpub.bind(f"{proto}://*:{xpub_port}")

    zmq.proxy(xpub, xsub)

    return

if __name__=='__main__':
    main(useWebSocket=False)
    #main(useWebSocket=True)
    pass
