
use std::env;
use std::ffi::OsString;

fn main() {
    #[cfg(feature="USE_WEBSOCKET")]
    let proto = "ws";

    #[cfg(not(feature="USE_WEBSOCKET"))]
    let proto = "tcp";

    let args: Vec<OsString> = env::args_os().collect();
    let port = if args.len()>1 { args[1].clone().into_string().unwrap().parse::<i32>().unwrap() } else { 2001 };

    let ctx = zmq::Context::new();

    let sub0 = ctx.socket(zmq::SUB).unwrap();
    let addr = format!("{}://localhost:{}", proto, port);
    println!("-- connect to {}", addr);
    sub0.connect(&addr).unwrap();
    sub0.set_subscribe(b"").unwrap();

    loop {
        let ret = sub0.recv_msg(0);
        match ret {
            Ok(msg) => match msg.as_str() {
                Some(str) => println!("-- {}", str),
                None      => println!("++ None"),
            }
            Err(v)  => println!("** {:?}", v),
        }
    }
}
