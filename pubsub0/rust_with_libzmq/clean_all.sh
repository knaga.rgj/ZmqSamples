#!/bin/sh -x

oscode=`lsb_release -sc`

for dir in prx0 pub0 sub0; do
    (cd ${dir}; cargo clean; rm ${dir}.*.${oscode} )
done

/bin/rm -f *~ */*~ */*/*~ */Cargo.lock 
