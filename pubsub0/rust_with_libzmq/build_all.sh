#!/bin/sh -x

oscode=`lsb_release -sc`

for sub in prx0 pub0 sub0 ; do
    (cd ${sub}; cargo build --release --features USE_WEBSOCKET; cp target/release/${sub} ${sub}.ws.${oscode} )
    (cd ${sub}; cargo build --release; cp target/release/${sub} ${sub}.tcp.${oscode} )
done
