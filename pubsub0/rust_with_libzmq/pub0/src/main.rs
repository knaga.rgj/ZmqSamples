
// extern crate getopts;

use std::env;
use std::process;
use std::ffi::OsString;
use getopts::Options;

fn print_usage(prg_name: &str, opts: &Options) {
    let msg = format!("Usage: {} [options]", prg_name);
    print!("{}", opts.usage(&msg));
    process::exit(0);
}

struct Args {
    use_connect: bool,
    port: i32,
}

fn parse_args() -> Args {
    let args: Vec<OsString> = env::args_os().collect();
    let prg_name = args[0].clone().into_string().unwrap();

    let mut opts = Options::new();
    opts.optopt("p", "port", "set output file name", "2000");
    opts.optflag("c", "connect", "connect to proxy, otherwise bind to the port");
    opts.optflag("h", "help", "print this help menu");

    //let matches = opts.parse(&args[1..])
    //    .unwrap_or_else(|f| panic!("{}", f.to_string()));
    let matches = opts.parse(&args[1..]).unwrap();

    if matches.opt_present("h") {
        print_usage(&prg_name, &opts);
    }

    let use_connect = if matches.opt_present("c") { true } else { false };

    let mut port : i32 = if use_connect { 2000 } else { 2001 };
    if matches.opt_present("p") {
        port = matches.opt_str("p").unwrap().parse::<i32>().unwrap();
    }

    //if matches.free.is_empty() {
    //    print_usage(&prg_name, &opts);
    //}

    Args {
        use_connect: use_connect,
        port:     port
    }
}



fn main() {
    let args = parse_args();

    #[cfg(feature="USE_WEBSOCKET")]
    let proto = "ws";

    #[cfg(not(feature="USE_WEBSOCKET"))]
    let proto = "tcp";

    let ctx = zmq::Context::new();

    let pub0 = ctx.socket(zmq::PUB).unwrap();

    if args.use_connect {
        let addr = format!("{}://localhost:{}", proto, args.port);
        println!("-- connect to {}", addr);
        pub0.connect(&addr).unwrap();
    } else {
        let addr = format!("{}://*:{}", proto, args.port);
        println!("-- bind {}", addr);
        pub0.bind(&addr).unwrap();
    }

    let mut count = 0;
    let half_sec = std::time::Duration::from_millis(500);
    loop {
        let msg = format!("Rust(zmq)/pub0 {}", count);
        pub0.send(&msg, 0).unwrap();
        count += 1;
        
        println!("{}", msg);
        std::thread::sleep(half_sec);
    }
}
