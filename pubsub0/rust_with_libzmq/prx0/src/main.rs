
use std::env;
use std::ffi::OsString;


fn main() {
    #[cfg(feature="USE_WEBSOCKET")]
    let proto = "ws";

    #[cfg(not(feature="USE_WEBSOCKET"))]
    let proto = "tcp";

    let args: Vec<OsString> = env::args_os().collect();
    let xsub_port = if args.len()>1 { args[1].clone().into_string().unwrap().parse::<i32>().unwrap() } else { 2000 };
    let xpub_port = xsub_port + 1;

    println!("-- proto: {proto}, xsub_port: {xsub_port}, xpub_port: {xpub_port}");

    let ctx = zmq::Context::new();

    let xsub = ctx.socket(zmq::XSUB).unwrap();
    let xsub_addr = format!("{}://*:{}", proto, xsub_port);
    xsub.bind(&xsub_addr).unwrap();

    let xpub = ctx.socket(zmq::XPUB).unwrap();
    let xpub_addr = format!("{}://*:{}", proto, xpub_port);
    xpub.bind(&xpub_addr).unwrap();

    zmq::proxy(&xpub, &xsub).unwrap();
}
