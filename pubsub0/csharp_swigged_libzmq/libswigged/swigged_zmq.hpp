//
//
//

#include <utility>  // pair, make_pair()
#include <string>
#include <zmq.h>

#include "swigged_bytebuf.hpp"

namespace Swigged {

class ZContext {
 public:
  void *ctx_;

  ZContext() : ctx_(zmq_ctx_new()) {}
  virtual ~ZContext() { close(); }

  void close(void) {
    if (ctx_ != ZMQ_NULL) {
      zmq_ctx_term(ctx_);
      ctx_ = ZMQ_NULL;
    }
  }
};  // class ZContext


class ZSocket {
 public:
  enum class Type {
    PAIR = 0,
    PUB = 1,
    SUB = 2,
    REQ = 3,
    REP = 4,
    DEALER = 5,
    ROUTER = 6,
    PULL = 7,
    PUSH = 8,
    XPUB = 9,
    XSUB = 10,
    STREAM = 11,
  };

  enum class Flag {
    NONE = 0,
    DONTWAIT = 1,
    SNDMORE = 2,
  };


  void *skt_;
  ZContext &ctx_;
  Type type_;

  ZSocket(ZContext &ctx, Type type) : skt_(ZMQ_NULL), ctx_(ctx), type_(type) { open(); }
  virtual ~ZSocket(void) { close(); }

  void open(void) {
    if (skt_ == ZMQ_NULL) {
      skt_ = zmq_socket(ctx_.ctx_, static_cast<int>(type_));
    }
  }

  void close(void) {
    if (skt_ != ZMQ_NULL) {
      zmq_close(skt_);
      skt_ = ZMQ_NULL;
    }
  }

  int getopt_CONNECT_TIMEOUT() {
    if (skt_ != ZMQ_NULL) {
      int ival;
      size_t sz;
      zmq_getsockopt(skt_, ZMQ_CONNECT_TIMEOUT, &ival, &sz);
      return ival;
    }
    return EINVAL;
  }

  int setopt_CONNECT_TIMEOUT(int ival) {
    size_t sz = sizeof(ival);
    return zmq_setsockopt(skt_, ZMQ_CONNECT_TIMEOUT, &ival, sz);
  }

  int setopt_SUBSCRIBE(const std::string &opt) {
    return zmq_setsockopt(skt_, ZMQ_SUBSCRIBE, opt.c_str(), opt.size());
  }


  int bind(const std::string &addr) {
    return zmq_bind(skt_, addr.c_str());
  }

  int connect(const std::string &addr) {
    return zmq_connect(skt_, addr.c_str());
  }

  int send(const std::string str, Flag flag) {
    return zmq_send(skt_, str.c_str(), str.size(), static_cast<int>(flag));
  }

  int send(ByteBuf &buf, Flag flag) {
    return zmq_send(skt_, buf.bytebuf_, buf.data_size_, static_cast<int>(flag));
  }

  int recv(ByteBuf &buf, Flag flag) {
    buf.data_size_ = zmq_recv(skt_, buf.bytebuf_, buf.buf_size_, static_cast<int>(flag));
    return buf.data_size_;
  }

}; // class ZSocket


int Zproxy (ZSocket &front, ZSocket &back);

}  // namespace Swigged


