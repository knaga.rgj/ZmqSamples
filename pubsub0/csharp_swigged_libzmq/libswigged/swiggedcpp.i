
%module SwiggedCpp
%{
#include "swigged_zmq.hpp"
%}
%include "arrays_csharp.i"
CSHARP_ARRAYS(char, byte)

%apply char INOUT[] {char *bytebuf}
%typemap(csclassmodifiers) Swigged::ByteBuf "public partial class"
%include "swigged_bytebuf.hpp"

%include "std_string.i"
%include "swigged_zmq.hpp"
