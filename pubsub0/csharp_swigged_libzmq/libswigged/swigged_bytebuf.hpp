//
//
//

#include <cstring>

namespace Swigged {

class ByteBuf {
 public:
  char *bytebuf_  ;
  const int buf_size_;
 public:
  int data_size_;
 public:
  ByteBuf(int size) : bytebuf_(NULL), buf_size_(size), data_size_(0) { bytebuf_ = static_cast<char *>(malloc(buf_size_)); }
  ~ByteBuf() { free(bytebuf_); }
  void CopyFrom(char *bytebuf, int size) { std::memcpy(bytebuf_, bytebuf, size); data_size_ = size; }
  void CopyTo(char *bytebuf) { std::memcpy(bytebuf, bytebuf_, data_size_); }
  void* GetVoidPtr(void) { return (void *)bytebuf_; }
};

}  // namespace Swigged
