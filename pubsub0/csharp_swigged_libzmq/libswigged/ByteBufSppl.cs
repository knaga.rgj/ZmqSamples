
namespace Swigged {

public partial class ByteBuf : global::System.IDisposable
{
  public void set(byte[] data)
  {
    unsafe
    {
      fixed (byte *datap = data)
      {
        this.CopyFrom(data, data.Length);
      }
    }
  }
  
  public unsafe Span<byte> get()
  {
    global::System.IntPtr cPtr = SwiggedCppPINVOKE.ByteBuf_GetVoidPtr(swigCPtr);
    return new Span<byte>((void*)cPtr, this.data_size_);
  }
  
}  // class ByteBuf

}  /// namespace Swigged
