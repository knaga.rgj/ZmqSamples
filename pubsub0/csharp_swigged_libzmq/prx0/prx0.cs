﻿// See https://aka.ms/new-console-template for more information

bool useWebSocket = false;

var proto = useWebSocket ? "ws" : "tcp";

var xsub_port = 2000;
var xpub_port = 2001;

var xsub_addr = String.Format("{0}://*:{1}", proto, xsub_port);
var xpub_addr = String.Format("{0}://*:{1}", proto, xpub_port);
    
var ctx = new Swigged.ZContext();

var xsub_skt = new Swigged.ZSocket(ctx, Swigged.ZSocket.Type.XSUB);
xsub_skt.bind(xsub_addr);

var xpub_skt = new Swigged.ZSocket(ctx, Swigged.ZSocket.Type.XPUB);
xpub_skt.bind(xpub_addr);

Swigged.SwiggedCpp.Zproxy(xpub_skt, xsub_skt);

Console.WriteLine("Hello, World!");
