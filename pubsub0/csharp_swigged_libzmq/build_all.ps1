
# Set-ExecutionPolicy remotesigned -scope process

$ZMQVER="4.3.5"
$ZMQTGZ="zeromq-{0}.tar.gz" -f $ZMQVER
$ZMQTGZPATH="archives\{0}" -f $ZMQTGZ

if (!(Test-Path $ZMQTGZPATH -PathType Leaf)) {
   Write-Host "Invoke-WebRequest https://github.com/zeromq/libzmq/releases/download/v${ZMQVER}/${ZMQTGZ} -OutFile ${ZMQTGZPATH}"
   Invoke-WebRequest https://github.com/zeromq/libzmq/releases/download/v${ZMQVER}/${ZMQTGZ} -OutFile ${ZMQTGZPATH}
}

if (!(Test-Path $ZMQTGZPATH -PathType Leaf)) {
  Write-Host "failed to get ${ZMQTGZ}"
  pause
  exit
}

Set-Location libswigged
tar xfz ../${ZMQTGZPATH}
swig -csharp -c++ -namespace Swigged -dllimport swiggedcpp swiggedcpp.i 
mkdir cbuild
Set-Location cbuild
cmake ..
cmake --build . --config Release
Set-Location ..
dotnet new classlib
Remove-Item Class1.cs
dotnet publish /p:AllowUnsafeBlocks=true

Set-Location ..\pub0
dotnet publish

Set-Location ..\sub0
dotnet publish

Set-Location ..\prx0
dotnet publish

Set-Location ..

pause
