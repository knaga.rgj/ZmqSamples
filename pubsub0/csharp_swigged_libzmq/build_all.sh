#!/bin/sh -x

ZMQVER=4.3.5
ZMQTGZ=zeromq-${ZMQVER}.tar.gz

if [ ! -f archives/${ZMQTGZ} ]; then
    wget -O archives/${ZMQTGZ} https://github.com/zeromq/libzmq/releases/download/v${ZMQVER}/${ZMQTGZ}
fi

(cd libswigged; tar xfz ../archives/${ZMQTGZ})

(cd libswigged/; swig -csharp -c++ -namespace Swigged -dllimport swiggedcpp swiggedcpp.i )
(mkdir -p libswigged/cbuild; cd libswigged/cbuild; cmake ..; cmake --build .)
(cd libswigged/; dotnet new classlib; /bin/rm -f Class1.cs )
(cd libswigged/; dotnet publish /p:AllowUnsafeBlocks=true )

(cd pub0; dotnet publish)
(cd sub0; dotnet publish)
(cd prx0; dotnet publish)

