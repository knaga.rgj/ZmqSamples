﻿// See https://aka.ms/new-console-template for more information

bool useProxy = true;
bool useWebSocket = false;

var proto = useWebSocket ? "ws" : "tcp";
var port = useProxy ? 2000 : 2001;

var utf8enc = System.Text.Encoding.GetEncoding("UTF-8");

var addr = String.Format("{0}://localhost:{1}", proto, port);
Console.WriteLine("addr: {0}", addr);

var ctx = new Swigged.ZContext();
var skt = new Swigged.ZSocket(ctx, Swigged.ZSocket.Type.PUB);
var buf = new Swigged.ByteBuf(8192);

var ret = useProxy ? skt.connect(addr) : skt.bind(addr);

int count=0;
while (true)
{
    var msg = String.Format("C#/swig/libzmq: {0}",count++);
    buf.set(utf8enc.GetBytes(msg));
  
    //skt.send(msg, 0);
    skt.send(buf, 0);

    Console.WriteLine(msg);
    System.Threading.Thread.Sleep(50);
}
    
// skt.close();
// ctx.term();

Console.WriteLine("Hello, World!");
