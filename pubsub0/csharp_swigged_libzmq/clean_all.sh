#!/bin/sh -x

/bin/rm -f libswigged/*.o libswigged/*.csproj  libswigged/*.so  libswigged/*_wrap.cxx
(cd libswigged; /bin/rm -f ByteBuf.cs SWIGTYPE_p_std__pairT_int_std__string_t.cs SWIGTYPE_p_void.cs SwiggedCpp.cs SwiggedCppPINVOKE.cs ZContext.cs ZSocket.cs)
/bin/rm -rf libswigged/bin libswigged/obj libswigged/cbuild libswigged/zeromq-4.3.5
/bin/rm -rf pub0/bin pub0/obj
/bin/rm -rf sub0/bin sub0/obj
/bin/rm -rf prx0/bin prx0/obj
/bin/rm -f *~ */*~
