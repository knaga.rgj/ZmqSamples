﻿// See https://aka.ms/new-console-template for more information

bool useWebSocket = false;

var proto = useWebSocket ? "ws" : "tcp";
var port = 2001;

var utf8enc = System.Text.Encoding.GetEncoding("UTF-8");

var addr = String.Format("{0}://localhost:{1}", proto, port);
Console.WriteLine("addr: {0}", addr);

var ctx = new Swigged.ZContext();
var skt = new Swigged.ZSocket(ctx, Swigged.ZSocket.Type.SUB);
var buf = new Swigged.ByteBuf(8192);

skt.connect(addr);
skt.setopt_SUBSCRIBE("");




while (true)
{
  skt.recv(buf, 0);
  var str = utf8enc.GetString(buf.get());
  Console.WriteLine(str);
  
  // ReadOnlySpan<byte> data = buf.bytebuf_;
  // var str = utf8enc.GetString(data.Slice(0, buf.data_size_));
  // Console.WriteLine(str);
  
  //unsafe {
  //  var str = utf8enc.GetString(buf.bytebuf_, 0, buf.data_size_);
  //  Console.WriteLine(str);
  //}
}


Console.WriteLine("Hello, World!");
