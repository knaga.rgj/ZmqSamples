
// ref: https://github.com/go-zeromq/zmq4/blob/main/example/psenvsub.go


package main

import (
	"fmt"
	"context"
	"github.com/go-zeromq/zmq4"
)

func main() {
	//useWebSocket := false  // WebSocket doesn't seems to be supported

	port := int(2001)

	proto := "tcp"
	//if useWebSocket {
	//	proto = "ws"
	//}

	addr := fmt.Sprintf("%s://localhost:%d", proto, port)
	sub := zmq4.NewSub(context.Background(), zmq4.WithDialerMaxRetries(-1),  zmq4.WithAutomaticReconnect(true))
	err := sub.Dial(addr)
	if (err != nil) {
		fmt.Printf("** failed to Dial(): %s\n", err)
	}
	
	sub.SetOption(zmq4.OptionSubscribe, "")
	for {
		msg, err := sub.Recv()
		if (err != nil) {
			fmt.Printf("** failed to Recv(): %s\n", err)
		} else {
			fmt.Printf("%s\n", msg.Frames[0])
		}
	}

}
