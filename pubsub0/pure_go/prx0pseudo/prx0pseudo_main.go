//
// this doesn't use xpub-xsub proxy because go-zeromq doesn't seem to supoort it.
//


package main

import (
	"fmt"
	"context"
	"github.com/go-zeromq/zmq4"
)


func main() {
	//useWebSocket := false  // WebSocket doesn't seems to be supported
	
	sub_port := int(2000)
	pub_port := int(2001)

	proto := "tcp"
	//if useWebSocket {
	//	proto = "ws"
	//}

	sub_addr := fmt.Sprintf("%s://*:%d", proto, sub_port)
	pub_addr := fmt.Sprintf("%s://*:%d", proto, pub_port)
	
	ctx := context.Background()

	sub := zmq4.NewSub(ctx)
	err  := sub.Listen(sub_addr)
	if (err != nil) {
		fmt.Printf("** failed to sub.Listen(%s): %s\n", sub_addr, err)
		return
	}
	sub.SetOption(zmq4.OptionSubscribe, "")

	pub := zmq4.NewPub(ctx)
	err   = pub.Listen(pub_addr)
	if (err != nil) {
		fmt.Printf("** failed to pub.Listen(%s): %s\n", pub_addr, err)
		return
	}

	for {
		msg, err := sub.Recv()
		if (err != nil) {
			fmt.Printf("** failed to Recv(): %s\n", err)
		} else {
			pub.Send(msg)
		}
	}
	
	return
}
