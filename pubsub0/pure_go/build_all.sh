#!/bin/sh -x

for dir in pub0 sub0 prx0pseudo; do
    ( cd ${dir}; go mod init ${dir} ; go get; go build )
done
