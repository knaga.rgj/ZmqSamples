
// ref: https://github.com/go-zeromq/zmq4/blob/main/example/psenvpub.go


package main

import (
	"fmt"
	"time"
	"context"
	"github.com/go-zeromq/zmq4"
)

func main() {
	useProxy := true
	//useWebSocket := false  // WebSocket doesn't seems to be supported

	port := int(2001)
	if useProxy {
		port = 2000
	}

	proto := "tcp"
	//if useWebSocket {
	//	proto = "ws"
	//}

	addr := fmt.Sprintf("%s://*:%d", proto, port)
	pub  := zmq4.NewPub(nil)
	err  := error(nil)
	
	if useProxy {
		pub = zmq4.NewPub(context.Background(), zmq4.WithDialerMaxRetries(-1),  zmq4.WithAutomaticReconnect(true))
		err = pub.Dial(addr)
		if (err != nil) {
			fmt.Printf("** failed to Dial(%s): %s\n", addr, err)
			return
		}

	} else {
		pub = zmq4.NewPub(context.Background())
		err = pub.Listen(addr)
		if (err != nil) {
			fmt.Printf("** failed to Listen(%s): %s\n", addr, err)
			return
		}
	}
	
	
	count := int(0)
	for {
		msg := fmt.Sprintf("go-zeromq/pub0 %d", count)
		pub.Send(zmq4.NewMsg([]byte(msg)))
		fmt.Printf(msg+"\n")
		time.Sleep(500 * time.Millisecond)
		count += 1
	}
}
