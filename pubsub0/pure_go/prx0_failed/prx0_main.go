//
// this doesn't work
//


package main

import (
	"fmt"
	"context"
	"github.com/go-zeromq/zmq4"
)


func main() {
	xsub_port := int(2000)
	xpub_port := int(2001)

	xsub_addr := fmt.Sprintf("tcp://*:%d", xsub_port)
	xpub_addr := fmt.Sprintf("tcp://*:%d", xpub_port)
	
	ctx := context.Background()

	xsub := zmq4.NewXSub(ctx)
	err  := xsub.Listen(xsub_addr)
	if (err != nil) {
		fmt.Printf("** failed to xsub.Listen(%s): %s\n", xsub_addr, err)
		return
	}

	xpub := zmq4.NewXPub(ctx)
	err   = xpub.Listen(xpub_addr)
	if (err != nil) {
		fmt.Printf("** failed to xpub.Listen(%s): %s\n", xpub_addr, err)
		return
	}

	proxy := zmq4.NewProxy(ctx, xsub, xpub, nil)
	//proxy := zmq4.NewProxy(ctx, xpub, xsub, nil)
	err = proxy.Run()
	if (err != nil) {
		fmt.Printf("** failed to proxy.Run(): %s\n", err)
	}

	return
}
