
// ref: https://github.com/pebbe/zmq4/blob/master/examples/wuproxy.go

package main

import(
	"fmt"
	//zmq "github.com/pebbe/zmq4/draft"
	zmq "github.com/pebbe/zmq4"
)


func main() {
	useWebSocket := false
	
	xsub_port := int(2000)
	xpub_port := int(2001)

	proto := "tcp"
	if useWebSocket {
		proto = "ws"
	}

	xsub_addr := fmt.Sprintf("%s://*:%d", proto, xsub_port)
	xpub_addr := fmt.Sprintf("%s://*:%d", proto, xpub_port)
	
	xsub, _ := zmq.NewSocket(zmq.XSUB)
	xsub.Bind(xsub_addr)

	xpub, _ := zmq.NewSocket(zmq.XPUB)
	xpub.Bind(xpub_addr)

	err := zmq.Proxy(xsub, xpub, nil)
	fmt.Printf("** failed to Proxy(): %s\n", err)

	return
}
