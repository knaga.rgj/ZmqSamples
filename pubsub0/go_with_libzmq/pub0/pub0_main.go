
// ref: https://github.com/pebbe/zmq4/blob/master/examples/psenvpub.go

package main

import(
	"fmt"
	"time"
	zmq "github.com/pebbe/zmq4"
)

func main() {
	useProxy := true
	useWebSocket := false

	port := int(2001)
	if useProxy {
		port = 2000
	}

	proto := "tcp"
	if useWebSocket {
		proto = "ws"
	}

	addr := fmt.Sprintf("%s://127.0.0.1:%d", proto, port)
	pub, _ := zmq.NewSocket(zmq.PUB)
	
	if useProxy {
		pub.Connect(addr)
	} else {
		pub.Bind(addr)
	}
	
	count := int(0)
	for {
		msg := fmt.Sprintf("go-libzmq/pub0 %d", count)
		pub.Send(msg, 0)
		fmt.Printf(msg+"\n")
		time.Sleep(500 * time.Millisecond)
		count += 1
	}
}
