#!/bin/sh -x

for dir in prx0 pub0 sub0; do
    ( cd ${dir}; go mod init ${dir} ; go get; go build )
done
