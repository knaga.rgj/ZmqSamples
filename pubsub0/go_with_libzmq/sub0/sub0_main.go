
// ref: https://github.com/pebbe/zmq4/blob/master/examples/psenvsub.go

package main

import(
	"fmt"
	zmq "github.com/pebbe/zmq4"
)


func main() {
	port := int(2001)
	useWebSocket := false

	proto := "tcp"
	if useWebSocket {
		proto = "ws"
	}
	
	addr := fmt.Sprintf("%s://localhost:%d", proto, port)
	sub, _ := zmq.NewSocket(zmq.SUB)
	sub.Connect(addr)
	sub.SetSubscribe("")
	for {
		msg, err := sub.Recv(0)
		if (err != nil) {
			fmt.Printf("** failed to Recv(): %s\n", err)
		} else {
			fmt.Printf("%s\n", msg)
		}
	}


}
