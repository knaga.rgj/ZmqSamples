﻿
using NetMQ;
using NetMQ.Sockets;

using (var xsub = new XSubscriberSocket("@tcp://*:2000"))
using (var xpub = new XPublisherSocket("@tcp://*:2001"))
{
  Console.WriteLine("Intermediary started, and waiting for messages");
  // proxy messages between frontend / backend
  var prx = new Proxy(xsub, xpub);
  // blocks indefinitely
  prx.Start();
}

