﻿
using NetMQ;
using NetMQ.Sockets;

bool useProxy=true;
//bool useWebSocket=false;

int port;
if (useProxy)
{
  port = 2000;
}
else
{
  port = 2001;
}

String proto = "tcp";  // 'ws' (WebSocket) is not supported NetMQ (v4.0.1.13)

using (var pub = new NetMQ.Sockets.PublisherSocket())
{
  if (useProxy)
  {
    var addr = String.Format("{0}://localhost:{1}", proto, port);
    Console.WriteLine(addr);
    pub.Connect(addr);
  }
  else
  {
    var addr = String.Format("{0}://*:{1}", proto, port);
    Console.WriteLine(addr);
    pub.Bind(addr);
  }

  int count=0;
  while (true)
  {
    var msg = String.Format("pure_C#/pub0 {0}", count);
    pub.SendFrame(msg);
    count += 1;
    Console.WriteLine(msg);
    Thread.Sleep(500);
  }
}
