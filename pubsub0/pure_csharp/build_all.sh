#!/bin/sh -x

for dir in pub0 sub0 prx0; do
    (cd ${dir}; dotnet publish)
done
