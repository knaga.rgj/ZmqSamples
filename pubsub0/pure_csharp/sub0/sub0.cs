﻿

using NetMQ;
using NetMQ.Sockets;

int  port = 2001;
//bool useWebSocket = false;

String proto = "tcp";  // 'ws' (WebSocket) is not supported NetMQ (v4.0.1.13)

using (var sub = new NetMQ.Sockets.SubscriberSocket())
{
  var addr = String.Format("{0}://localhost:{1}", proto, port);
  Console.WriteLine(addr);
  sub.Connect(addr);
  sub.Subscribe("");

  while (true)
  {
    var msg = sub.ReceiveFrameString();
    Console.WriteLine(msg);
  }
}
