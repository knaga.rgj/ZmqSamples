#!/usr/bin/env python3

import nacl.public
import nacl.secret
import nacl.signing
import binascii
import msgpack
import zmq
from datetime import datetime

class MySubscriber:
    def __init__(self, no, dpath, zprt):
        self.no = no  # ID number of subscribers
        self.load(dpath)
        self.zctx = zmq.Context()
        self.zskt = self.zctx.socket(zmq.SUB)
        self.zskt.connect(zprt)
        self.zskt.setsockopt_string(zmq.SUBSCRIBE, '')
        return


    def load(self, dpath):
        with open(dpath, "rb") as f:
            line = f.readline()[:-1]
            vk = nacl.public.PrivateKey(binascii.unhexlify(line))
            line = f.readline()[:-1]
            bk = nacl.public.PublicKey(binascii.unhexlify(line))
            self.box = nacl.public.Box(vk, bk)
            line = f.readline()[:-1]
            self.vk = nacl.signing.VerifyKey(binascii.unhexlify(line))
            pass
        return


    def unpack(self, fdat):
        (subs, encd, sign) = msgpack.unpackb(fdat)
        try:
            k = self.box.decrypt(subs[self.no])
            #print("shrd: %s"%(binascii.hexlify(k)))
        except Exception as e:
            print(f"** failed to decrypt1: {e}")
            return (False, None)

        try:
            bdat = nacl.secret.SecretBox(k).decrypt(encd)
            #print("bdat: %s"%(binascii.hexlify(bdat)))
            #print("sign: %s"%(binascii.hexlify(sign)))
        except Exception as e:
            print(f"** failed to decrypt2: {e}")
            return (False, None)
            return 

        try:
            self.vk.verify(bdat, sign)
        except Exception as e:
            print(f"** failed to verify: {e}")
            return (False, None)
            pass
        
        return (True, bdat)


    def receive(self):
        (ret, bdat) = self.unpack(self.zskt.recv())
        if ret:
            dat = msgpack.unpackb(bdat)
        else:
            dat = None
        return (ret, dat)


    def from_file(self, fname):
        with open(fname, 'rb') as f:
            (ret, bdat) = self.unpack(f.read())
            if ret:
                dat = msgpack.unpackb(bdat)
                print(dat)
                pass
            pass
        return

    pass


def test0(no, dpath):
    sub = MySubscriber(no, dpath, "tcp://127.0.0.1:2001")
    sub.from_file("test.dat")
    return


def test1(no):
    dpath = "data/cons%d.dat"%(no)
    #sub = MySubscriber(no, dpath, "tcp://127.0.0.1:2001")
    sub = MySubscriber(no, dpath, "ws://127.0.0.1:2001")
    while True:
        (ret, dat) = sub.receive()
        now = datetime.now()
        if ret:
            print("-- [%s] got '%s'"%(now.strftime("%H:%M:%S"), str(dat)))
            pass
        pass

    return


def main(no):
    print("-- No.%d consumer"%(no))
    dpath = "data/cons%d.dat"%(no)
    sub = MySubscriber(no, dpath, "ws://sub.example.com:80")
    while True:
        (ret, dat) = sub.receive()
        now = datetime.now()
        if ret:
            print("-- [%s] got '%s'"%(now.strftime("%H:%M:%S"), str(dat)))
            pass
        pass

    return


if __name__ == '__main__':
    import sys
    #test0(int(sys.argv[1]), sys.argv[2])
    #test1(int(sys.argv[1]))
    no = int(sys.argv[1]) if len(sys.argv)>1 else 0
    main(no)
    pass
