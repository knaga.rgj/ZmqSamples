#! /usr/bin/env python3

import zmq


def main(useWebSocket=False):

    sub_port = 2000
    pub_port = 2001

    if useWebSocket:
        proto = "ws"
    else:
        proto = "tcp"
        pass

    ctx = zmq.Context()

    # Socket facing producers
    xsub = ctx.socket(zmq.XSUB)
    xsub.bind("%s://*:%d"%(proto, sub_port))

    # Socket facing consumers
    xpub = ctx.socket(zmq.XPUB)
    xpub.bind("%s://*:%d"%(proto, pub_port))

    zmq.proxy(xpub, xsub)

    return

if __name__=='__main__':
    #main(useWebSocket=False)
    main(useWebSocket=True)
    pass
