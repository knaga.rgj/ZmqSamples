#!/usr/bin/env python3

import nacl.public
import nacl.secret
import nacl.signing
import nacl.hash
import binascii
import msgpack
import zmq
from datetime import datetime
import time

class MyPublisher:
    def __init__(self, dpath, zprt):
        self.boxes=[]
        self.load(dpath)
        self.zctx = zmq.Context()
        self.zskt = self.zctx.socket(zmq.PUB)
        self.zskt.connect(zprt)
        return


    def load(self, dpath):
        with open(dpath, "rb") as f:
            line = f.readline()[:-1]
            vk = nacl.public.PrivateKey(binascii.unhexlify(line))
            line = f.readline()[:-1]
            self.nk = nacl.signing.SigningKey(binascii.unhexlify(line))
            for line in f:
                line = line[:-1]
                pk = nacl.public.PublicKey(binascii.unhexlify(line))
                self.boxes.append(nacl.public.Box(vk, pk))
                pass
            pass
        return


    def pack(self, bdat):
        # signature
        sign = self.nk.sign(bdat).signature
        #print("sign: %s"%(binascii.hexlify(sign)))
        #print("bdat: %s"%(binascii.hexlify(bdat)))

        if False:
            signed = self.nk.sign(bdat)
            print("smsg: %s"%(binascii.hexlify(signed.message)))
            print("sign: %s"%(binascii.hexlify(signed.signature)))

        # shared secret key
        k = nacl.utils.random(nacl.secret.SecretBox.KEY_SIZE)
        #print("shrd: %s"%(binascii.hexlify(k)))
        s = nacl.secret.SecretBox(k)
        subs = []
        for bx in self.boxes:
            subs.append(bx.encrypt(k))
            pass
        
        frame = [subs]
        frame.append(s.encrypt(bdat))
        frame.append(sign)
        return msgpack.packb(frame)


    def publish(self, data):
        return self.zskt.send(self.pack(msgpack.packb(data)))


    def dump(self, data, fname):
        with open(fname, "wb") as of:
            of.write(self.pack(msgpack.packb(data)))
            pass
        return

    pass


def test0():
    pub = MyPublisher("data/prod.dat", "tcp://127.0.0.1:2000")
    pub.dump(["test", 100], "test.dat")
    return


def test1():
    pub = MyPublisher("data/prod.dat", "ws://127.0.0.1:2000")
    #pub = MyPublisher("data/prod.dat", "tcp://127.0.0.1:2000")
    count = 0
    while True:
        data = ["test", count]
        count += 1
        pub.publish(data)
        now = datetime.now()
        print("-- [%s] published '%s'"%(now.strftime("%H:%M:%S"), str(data)))
        time.sleep(0.5)
        pass
    return


def main():
    pub = MyPublisher("data/prod.dat", "ws://pub.example.com:80")
    count = 0
    while True:
        data = ["test", count]
        count += 1
        pub.publish(data)
        now = datetime.now()
        print("-- [%s] published '%s'"%(now.strftime("%H:%M:%S"), str(data)))
        time.sleep(0.5)
        pass
    return


if __name__=='__main__':
    #test0()
    #test1()
    main()
    pass
