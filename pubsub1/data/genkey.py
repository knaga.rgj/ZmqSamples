#!/usr/bin/env python3

import nacl.public
import nacl.signing
import binascii

def genpub():
    k=nacl.public.PrivateKey.generate()
    prv = binascii.hexlify(k._private_key)
    pub = binascii.hexlify(bytes(k.public_key))
    return (prv, pub)

def gensig():
    k=nacl.signing.SigningKey.generate()
    seed = binascii.hexlify(k._seed)
    pub  = binascii.hexlify(k.verify_key._key)
    return (seed, pub)

def genkey(num):
    prod_pub = genpub()
    prod_sig = gensig()

    subs = []
    for idx in range(num):
        subs.append(genpub())
        pass

    fname = "prod.dat"
    with open(fname, "wb") as f:
        f.write(prod_pub[0])
        f.write(b'\n')
        f.write(prod_sig[0])
        f.write(b'\n')
        for sub in subs:
            f.write(sub[1])
            f.write(b'\n')
            pass
        pass
    print(f"-- {fname} generated")

    for idx in range(num):
        fname = "cons%d.dat"%(idx)
        with open(fname, "wb") as f:
            f.write(subs[idx][0])
            f.write(b'\n')
            f.write(prod_pub[1])
            f.write(b'\n')
            f.write(prod_sig[1])
            f.write(b'\n')
            pass
        print(f"-- {fname} generated")
        pass

    return


def genkey0(basename):
    prv_path = basename+"v.dat"
    pub_path = basename+"b.dat"

    k=nacl.public.PrivateKey.generate()
    vstr = binascii.hexlify(k._private_key).decode()
    bstr = binascii.hexlify(bytes(k.public_key)).decode()

    with open(prv_path, "w") as of:
        of.write(vstr)
        of.write("\n")
        pass
    print("-- %s saved."%(prv_path))

    with open(pub_path, "w") as of:
        of.write(bstr)
        of.write("\n")
        pass
    print("-- %s saved."%(pub_path))

    return

if __name__ == '__main__':
    import sys
    #genkey0(sys.argv[1])
    genkey(int(sys.argv[1]))
    pass
