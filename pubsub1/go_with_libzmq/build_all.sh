#!/bin/sh -x

for dir in prx0; do
    ( cd ${dir}; go mod init ${dir} ; go get; go build )
done
