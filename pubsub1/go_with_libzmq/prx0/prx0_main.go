
// ref: https://github.com/pebbe/zmq4/blob/master/examples/wuproxy.go

package main

import(
	"fmt"
	//zmq "github.com/pebbe/zmq4/draft"
	zmq "github.com/pebbe/zmq4"
)


func main() {
	useWebSocket := true
	
	xsub_port := int(2000)
	xpub_port := int(2001)

	proto := "tcp"
	if useWebSocket {
		proto = "ws"
	}

	xsub, _ := zmq.NewSocket(zmq.XSUB)
	xpub, _ := zmq.NewSocket(zmq.XPUB)
	
	xsub_addr := fmt.Sprintf("%s://*:%d", proto, xsub_port)
	xsub.Bind(xsub_addr)

	xpub_addr := fmt.Sprintf("%s://*:%d", proto, xpub_port)
	xpub.Bind(xpub_addr)

	err := zmq.Proxy(xsub, xpub, nil)
	fmt.Printf("** failed to Proxy(): %s\n", err)

	return
}

// Emacs Settings
// Local Variables:
//  eval: (outline-minor-mode)
//  tab-width: 4
//  c-basic-offset: 2
// End:


